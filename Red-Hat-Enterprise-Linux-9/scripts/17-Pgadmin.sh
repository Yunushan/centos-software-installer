#!/bin/bash

#17-pgAdmin
pgadmin_latest=$(lynx -dump https://ftp.postgresql.org/pub/pgadmin/pgadmin4/yum | awk '/http/{print $2}' | grep -i redhat-repo | head -n 1)
sudo rpm -Uvh "$pgadmin_latest"
sudo dnf -vy install pgadmin4
sudo /usr/pgadmin4/bin/setup-web.sh
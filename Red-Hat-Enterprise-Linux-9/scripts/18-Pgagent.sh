#!/bin/bash

#18-pgAgent

pgagent_version_1=$(lynx -dump https://download.postgresql.org/pub/repos/yum/ | awk '/http/{print $2}' \
| grep -iv "common\|debug\|keys\|non-free\|reporpms\|srpms\|testing" | tail -n 5 | sed -n '1 p' | cut -c 47-48)


pgagent_version_2=$(lynx -dump https://download.postgresql.org/pub/repos/yum/ | awk '/http/{print $2}' \
| grep -iv "common\|debug\|keys\|non-free\|reporpms\|srpms\|testing" | tail -n 5 | sed -n '2 p' | cut -c 47-48)


pgagent_version_3=$(lynx -dump https://download.postgresql.org/pub/repos/yum/ | awk '/http/{print $2}' \
| grep -iv "common\|debug\|keys\|non-free\|reporpms\|srpms\|testing" | tail -n 5 | sed -n '3 p' | cut -c 47-48)


pgagent_version_4=$(lynx -dump https://download.postgresql.org/pub/repos/yum/ | awk '/http/{print $2}' \
| grep -iv "common\|debug\|keys\|non-free\|reporpms\|srpms\|testing" | tail -n 5 | sed -n '4 p' | cut -c 47-48)


pgagent_version_5=$(lynx -dump https://download.postgresql.org/pub/repos/yum/ | awk '/http/{print $2}' \
| grep -iv "common\|debug\|keys\|non-free\|reporpms\|srpms\|testing" | tail -n 5 | sed -n '5 p' | cut -c 47-48)


printf "\nPlease Choose Your Desired PgAgent Version\n\n1-)PgAgent ""$pgagent_version_1""\n2-)PgAgent ""$pgagent_version_2""\n\
3-)PgAgent ""$pgagent_version_3""\n4-)PgAgent ""$pgagent_version_4""\n5-)PgAgent ""$pgagent_version_5""\n\nPlease Select Your PgAgent Version:"
read -r pgagent_version

pgagent_version_2=$(lynx -dump https://download.postgresql.org/pub/repos/yum/"$pgagent_version_2"/redhat/rhel-9-x86_64/ | awk '/http/{print $2}' | grep -i pgagent | tail -n 1)
pgagent_version_3=$(lynx -dump https://download.postgresql.org/pub/repos/yum/"$pgagent_version_3"/redhat/rhel-9-x86_64/ | awk '/http/{print $2}' | grep -i pgagent | tail -n 1)
pgagent_version_4=$(lynx -dump https://download.postgresql.org/pub/repos/yum/"$pgagent_version_4"/redhat/rhel-9-x86_64/ | awk '/http/{print $2}' | grep -i pgagent | tail -n 1)
pgagent_version_5=$(lynx -dump https://download.postgresql.org/pub/repos/yum/"$pgagent_version_5"/redhat/rhel-9-x86_64/ | awk '/http/{print $2}' | grep -i pgagent | tail -n 1)
if [ "$pgagent_version" = "1" ];then
    pgagent_version_1=$(lynx -dump https://download.postgresql.org/pub/repos/yum/"$pgagent_version_1"/redhat/rhel-9-x86_64/ | awk '/http/{print $2}' | grep -i pgagent | tail -n 1)
    sudo dnf -vy install boost-system boost-filesystem boost-atomic boost-chrono boost-thread boost-date-time libpq
    sudo rpm -Uvh "$pgagent_version_1"
    pgagent_version_1=$(systemctl list-unit-files | grep -i pgagent | head -n 1 | cut -c 9-10)
    sudo systemctl enable pgagent_"$pgagent_version_1".service
    sudo systemctl start pgagent_"$pgagent_version_1".service
elif [ "$pgagent_version" = "2" ];then
    pgagent_version_2=$(lynx -dump https://download.postgresql.org/pub/repos/yum/"$pgagent_version_2"/redhat/rhel-9-x86_64/ | awk '/http/{print $2}' | grep -i pgagent | tail -n 1)
    sudo dnf -vy install boost-system boost-filesystem boost-atomic boost-chrono boost-thread boost-date-time libpq
    sudo rpm -Uvh "$pgagent_version_2"
    pgagent_version_2=$(systemctl list-unit-files | grep -i pgagent | head -n 1 | cut -c 9-10)
    sudo systemctl enable pgagent_"$pgagent_version_2".service
    sudo systemctl start pgagent_"$pgagent_version_2".service
elif [ "$pgagent_version" = "3" ];then
    pgagent_version_3=$(lynx -dump https://download.postgresql.org/pub/repos/yum/"$pgagent_version_3"/redhat/rhel-9-x86_64/ | awk '/http/{print $2}' | grep -i pgagent | tail -n 1)
    sudo dnf -vy install boost-system boost-filesystem boost-atomic boost-chrono boost-thread boost-date-time libpq
    sudo rpm -Uvh "$pgagent_version_3"
    pgagent_version_3=$(systemctl list-unit-files | grep -i pgagent | head -n 1 | cut -c 9-10)
    sudo systemctl enable pgagent_"$pgagent_version_3".service
    sudo systemctl start pgagent_"$pgagent_version_3".service
elif [ "$pgagent_version" = "4" ];then
    pgagent_version_4=$(lynx -dump https://download.postgresql.org/pub/repos/yum/"$pgagent_version_4"/redhat/rhel-9-x86_64/ | awk '/http/{print $2}' | grep -i pgagent | tail -n 1)
    sudo dnf -vy install boost-system boost-filesystem boost-atomic boost-chrono boost-thread boost-date-time libpq
    sudo rpm -Uvh "$pgagent_version_4"
    pgagent_version_4=$(systemctl list-unit-files | grep -i pgagent | head -n 1 | cut -c 9-10)
    sudo systemctl enable pgagent_"$pgagent_version_4".service
    sudo systemctl start pgagent_"$pgagent_version_4".service
elif [ "$pgagent_version" = "5" ];then
    pgagent_version_5=$(lynx -dump https://download.postgresql.org/pub/repos/yum/"$pgagent_version_5"/redhat/rhel-9-x86_64/ | awk '/http/{print $2}' | grep -i pgagent | tail -n 1)
    sudo dnf -vy install boost-system boost-filesystem boost-atomic boost-chrono boost-thread boost-date-time libpq
    sudo rpm -Uvh "$pgagent_version_5"
    pgagent_version_5=$(systemctl list-unit-files | grep -i pgagent | head -n 1 | cut -c 9-10)
    sudo systemctl enable pgagent_"$pgagent_version_5".service
    sudo systemctl start pgagent_"$pgagent_version_5".service
else
    echo "Out of options please choose between 1-5"
fi
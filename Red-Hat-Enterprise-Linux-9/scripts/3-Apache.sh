#!/bin/bash

#3-Apache2
printf "\nPlease Choose Your Desired Apache Version\n\n1-) Apache (From Official Package)\n\
2-) Apache Latest(Compile From Source)\n3-) Apache Latest (From .rpm file with default spec file)\n\
4-) Apache Latest (From .rpm file with custom .spec file only --prefix=/etc/httpd added)\
\n\nPlease Select Your Apache Version:"
read -r apacheversion
if [ "$apacheversion" = "1" ];then
    sudo dnf -vy install apache2
    sudo systemctl start httpd
    sudo systemctl enable httpd
elif [ "$apacheversion" = "2" ];then
    sudo dnf -vy install gcc pcre-devel make cmake redhat-rpm-config doxygen libtool
    #apr_latest=$(lynx -dump https://dlcdn.apache.org/apr/ | awk /http/'{print $2}' | grep -iv "asc\|sha256\|iconv\|util" | grep -i .tar.gz | tail -n 1)
    #apr_latest_folder_name=$(lynx -dump https://dlcdn.apache.org/apr/ | awk /http/'{print $2}' | grep -iv "asc\|sha256\|iconv\|util" \
    #| grep -i .tar.gz | tail -n 1 | grep -Po 'apr/\K.*' | sed 's/.tar.gz//g')
    #wget -O /root/Downloads/"$apr_latest_folder_name".tar.gz "$apr_latest"
    #apr_util_latest=$(lynx -dump https://dlcdn.apache.org/apr/ | awk /http/'{print $2}' | grep -iv "asc\|sha256\|iconv\|changes\|aprutil\|.zip" \
    #| grep -i ".tar.gz" | grep -i "util" | head -n 1)
    #apr_util_latest_folder_name=$(lynx -dump https://dlcdn.apache.org/apr/ | awk /http/'{print $2}' | grep -iv "asc\|sha256\|iconv" | grep -i .tar.gz \
    #| tail -n 1 | grep -Po 'apr/\K.*' | sed 's/.tar.gz//g')
    #mkdir -pv /root/Downloads/"$apr_latest_folder_name"
    #tar -xvf /root/Downloads/"$apr_latest_folder_name".tar.gz -C /root/Downloads/"$apr_latest_folder_name" --strip-components 1
    #wget -O /root/Downloads/"$apr_util_latest_folder_name".tar.gz "$apr_util_latest"
    #mkdir -pv /root/Downloads/"$apr_util_latest_folder_name"
    #tar -xvf  /root/Downlaods/"$apr_util_latest_folder_name".tar.gz -C /root/Downloads/"$apr_util_latest_folder_name" --strip-components 1
    #cd /root/Downloads/"$apr_latest_folder_name" || exit 2
    #touch /root/Downloads/"$apr_latest_folder_name"/libtoolT
    #./configure
    #make -j "${core:=}"
    #make install
    #sleep 2
    #cd /root/Downloads/"$apr_util_latest_folder_name" || exit 2
    #./configure --with-apr=/root/downloads/"$apr_latest_folder_name"
    #make -j "$core"
    #make install
    
    #libexpat_latest=$(lynx -dump https://github.com/libexpat/libexpat/tags | awk /http/'{print $2}' | grep -i '.tar.gz' | head -n 1)
    #libexpat_latest_folder_name=$(lynx -dump https://github.com/libexpat/libexpat/tags | awk /http/'{print $2}' | grep -i '.tar.gz' | head -n 1 \
    #| grep -Po 'R_\K.*' | sed 's/.tar.gz//g')
    #mkdir -pv /root/Downloads/libexpat"$libexpat_latest_folder_name"
    #wget -O /root/Downloads/libexpat"$libexpat_latest_folder_name".tar.gz "$libexpat_latest"
    #tar -xvf /root/Downloads/libexpat"$libexpat_latest_folder_name".tar.gz -C /root/Downloads/libexpat"$libexpat_latest_folder_name" --strip-components 1
    #cd /root/Downloads/libexpat"$libexpat_latest_folder_name" || exit 2
    
    apache_latest=$(lynx -dump https://dlcdn.apache.org//httpd | awk '{print $2}' | grep -iv '.asc\|.sha' \
    | grep -i .tar.gz | tail -n 1)
    apache_latest_folder_name=$(lynx -dump https://dlcdn.apache.org//httpd | awk '{print $2}' | grep -iv '.asc\|.sha' | grep -i .tar.gz \
    | tail -n 1 | grep -Po 'httpd/\K.*' | sed 's/.tar.gz//g')
    mkdir -pv /root/Downloads
    mkdir -pv /usr/local/apache
    wget -O /root/Downloads/"$apache_latest_folder_name".tar.gz "$apache_latest"
    mkdir -pv /root/Downloads/"$apache_latest_folder_name"
    tar -xvf /root/Downloads/"$apache_latest_folder_name".tar.gz -C /root/Downloads/"$apache_latest_folder_name" --strip-components 1
    cd /root/Downloads/"$apache_latest_folder_name" || exit 2
    ./configure --enable-ssl \
                --enable-so \
                --with-mpm=event \
                --with-included-apr \
                --enable-mods-shared=all \
                --prefix=/usr/local/apache

    make -j "${core:=}" && make -j "${core:=}" install

echo "[Unit]
Description=The Apache HTTP Server

[Service]
Type=forking
ExecStart=/usr/local/apache/bin/apachectl start
ExecReload=/usr/local/apache/bin/apachectl graceful
ExecStop=/usr/local/apache/bin/apachectl stop
PrivateTmp=true


[Install]
WantedBy=multi-user.target" > /etc/systemd/system/httpd.service

echo "pathmunge /usr/local/apache/bin" > /etc/profile.d/httpd.sh
ln -s /usr/local/apache/bin/httpd /usr/sbin/httpd
systemctl enable httpd
systemctl start httpd

elif [ "$apacheversion" = "3" ];then
    sudo dnf -vy remove httpd
    sudo mkdir -pv /root/Downloads/httpd
    apache_latest=$(lynx -dump https://dlcdn.apache.org//httpd | awk '{print $2}' | grep -iv '.asc\|.sha' | grep -i .tar.bz2 | tail -n 1)
    apache_latest_folder_name=$(lynx -dump https://dlcdn.apache.org//httpd | awk '{print $2}' | grep -iv '.asc\|.sha' | grep -i .tar.gz \
    | tail -n 1 | grep -Po 'httpd\K.*' | sed 's/.tar.gz//g' | cut -c 2-)
    apache_latest_version_name=$(lynx -dump https://dlcdn.apache.org//httpd | awk '{print $2}' | grep -iv '.asc\|.sha' | grep -i .tar.gz     | tail -n 1 | grep -Po 'httpd\K.*' | sed 's/.tar.gz//g' | cut -c 8-20)
    wget -O /root/Downloads/"$apache_latest_folder_name".tar.bz2 "$apache_latest"
    sudo dnf -vy install autoconf libuuid-devel lua libxml2-devel apr apr-util apr-util-devel perl make cmake gcc rpm-build rpmdevtools \
    rpmlint pcre-devel libselinux-devel openssl-devel lua-devel
    rpmdev-setuptree
    cd /root/Downloads/httpd || exit 2
    rpmbuild -tb "$apache_latest_folder_name".tar.bz2
    sudo dnf -vy install /root/rpmbuild/RPMS/x86_64/"$apache_latest_folder_name"-1.x86_64.rpm
    sudo dnf -vy install /root/rpmbuild/RPMS/x86_64/mod_ssl-"$apache_latest_version_name"-1.x86_64.rpm
    sudo dnf -vy install /root/rpmbuild/RPMS/x86_64/httpd-devel-"$apache_latest_version_name"-1.x86_64.rpm
    sudo dnf -vy install /root/rpmbuild/RPMS/x86_64/httpd-tools-"$apache_latest_version_name"-1.x86_64.rpm
    systemctl enable httpd
    systemctl start httpd
elif [ "$apacheversion" = "4" ];then
    sudo dnf -vy remove httpd
    sudo dnf -vy install rpm-build rpmdevtools rpmlint openssl-devel
    rpmdev-setuptree
    sudo mkdir -pv /root/rpmbuild/SOURCES/"$apache_latest_folder_name"
    wget -O /root/rpmbuild/SOURCES/"$apache_latest_folder_name".tar.bz2 https://dlcdn.apache.org//httpd/"$apache_latest_folder_name".tar.bz2
    tar -xvf /root/rpmbuild/SOURCES/"$apache_latest_folder_name".tar.bz2 -C /root/rpmbuild/SOURCES/"$apache_latest_folder_name" --strip-components 1
    sudo dnf -vy install autoconf libuuid-devel lua \
    libxml2-devel apr apr-util apr-util-devel \
    perl make cmake gcc rpm-build rpmdevtools rpmlint pcre-devel libselinux-devel lua-devel
    sudo cp -v /root/rpmbuild/SOURCES/"$apache_latest_folder_name"/httpd.spec /root/rpmbuild/SPECS/
    grep -qxF '        --prefix=/etc/httpd \ ' /root/rpmbuild/SPECS/httpd.spec || \
    sudo sed -i '/--enable-case-filter/a \ \ \ \ \ \ \ \ --prefix=/etc/httpd \\ ' /root/rpmbuild/SPECS/httpd.spec
    rpmbuild -ba /root/rpmbuild/SPECS/httpd.spec
    sudo dnf -vy install /root/rpmbuild/RPMS/x86_64/"$apache_latest_folder_name"-1.x86_64.rpm
    sudo dnf -vy install /root/rpmbuild/RPMS/x86_64/mod_ssl-"$apache_latest_version_name"-1.x86_64.rpm
    sudo dnf -vy install /root/rpmbuild/RPMS/x86_64/httpd-devel-"$apache_latest_version_name"-1.x86_64.rpm
    sudo dnf -vy install /root/rpmbuild/RPMS/x86_64/httpd-tools-"$apache_latest_version_name"-1.x86_64.rpm
    systemctl enable httpd
    systemctl start httpd
else
    echo "Out of options please choose between 1-4"
fi

sudo systemctl daemon-reload
sudo systemctl enable httpd
sudo systemctl start httpd
sudo dnf -vy install apachetop

#sudo firewall-cmd --zone=public --permanent --add-service=http
#sudo firewall-cmd --zone=public --permanent --add-service=https
#sudo firewall-cmd --reload
printf "\nApache2 Installation Has Finished\n\n"

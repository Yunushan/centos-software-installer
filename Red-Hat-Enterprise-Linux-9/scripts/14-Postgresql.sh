#!/bin/bash

#14-PostgreSQL
echo "Caching Postgresql version names..."
postgresql_version_1=$(dnf -q search postgresql | grep -iv "profiler\|rollback\|provide\|private\|proftpd\|tuned\|libpg\|pg_activity\|pgpool\|postgresql-server" \
| grep -i "-server" | cut -c 11-12 | sed -n '1 p')
postgresql_version_2=$(dnf -q search postgresql | grep -iv "profiler\|rollback\|provide\|private\|proftpd\|tuned\|libpg\|pg_activity\|pgpool\|postgresql-server" \
| grep -i "-server" | cut -c 11-12 | sed -n '2 p')
postgresql_version_3=$(dnf -q search postgresql | grep -iv "profiler\|rollback\|provide\|private\|proftpd\|tuned\|libpg\|pg_activity\|pgpool\|postgresql-server" \
| grep -i "-server" | cut -c 11-12 | sed -n '3 p')
postgresql_version_4=$(dnf -q search postgresql | grep -iv "profiler\|rollback\|provide\|private\|proftpd\|tuned\|libpg\|pg_activity\|pgpool\|postgresql-server" \
| grep -i "-server" | cut -c 11-12 | sed -n '4 p')
postgresql_version_5=$(dnf -q search postgresql | grep -iv "profiler\|rollback\|provide\|private\|proftpd\|tuned\|libpg\|pg_activity\|pgpool\|postgresql-server" \
| grep -i "-server" | cut -c 11-12 | sed -n '5 p')
printf "\nPlease Choose Your Desired PostgreSQL Version\n\n1-)PostgreSQL ""$postgresql_version_1""\n2-)PostgreSQL ""$postgresql_version_2""\n\
3-)PostgreSQL ""$postgresql_version_3""\n4-)PostgreSQL ""$postgresql_version_4""\n5-)PostgreSQL ""$postgresql_version_5""\n\nPlease Select Your PostgreSQL Version:"
read -r postgresql_version
sudo dnf -vy install https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
sudo dnf -vy module disable postgresql
sudo dnf clean all
if [ "$postgresql_version" = "1" ];then
    sudo dnf -vy install postgresql"$postgresql_version_1"-server postgresql"$postgresql_version_1"
    sudo /usr/pgsql-"$postgresql_version_1"/bin/postgresql-"$postgresql_version_1"-setup initdb
    sudo systemctl enable postgresql-"$postgresql_version_1"
    sudo systemctl start postgresql-"$postgresql_version_1"
elif [ "$postgresql_version" = "2" ];then
    sudo dnf -vy install postgresql"$postgresql_version_2"-server postgresql"$postgresql_version_2"
    sudo /usr/pgsql-"$postgresql_version_2"/bin/postgresql-"$postgresql_version_2"-setup initdb
    sudo systemctl enable postgresql-"$postgresql_version_2"
    sudo systemctl start postgresql-"$postgresql_version_2"
elif [ "$postgresql_version" = "3" ];then
    sudo dnf -vy install postgresql"$postgresql_version_3"-server postgresql"$postgresql_version_3"
    sudo /usr/pgsql-"$postgresql_version_3"/bin/postgresql-"$postgresql_version_3"-setup initdb
    sudo systemctl enable postgresql-"$postgresql_version_3"
    sudo systemctl start postgresql-"$postgresql_version_3"
elif [ "$postgresql_version" = "4" ];then
    sudo dnf -vy install postgresql"$postgresql_version_4"-server postgresql"$postgresql_version_4"
    sudo /usr/pgsql-"$postgresql_version_4"/bin/postgresql-"$postgresql_version_4"-setup initdb
    sudo systemctl enable postgresql-"$postgresql_version_4"
    sudo systemctl start postgresql-"$postgresql_version_4"
elif [ "$postgresql_version" = "5" ];then
    sudo dnf -vy install postgresql"$postgresql_version_5"-server postgresql"$postgresql_version_5"
    sudo /usr/pgsql-"$postgresql_version_5"/bin/postgresql-"$postgresql_version_5"-setup initdb
    sudo systemctl enable postgresql-"$postgresql_version_5"
    sudo systemctl start postgresql-"$postgresql_version_5"
else
    echo "Out of options please choose between 1-5"
fi
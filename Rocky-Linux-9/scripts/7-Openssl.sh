#!/bin/bash

# 7-) OpenSSL Installation Section

if [ "$opensslversion" = "1" ];then # 1-)OpenSSL 3 LTS (Official Package)
    sudo dnf -vy install openssl openssl-devel openssl-libs
elif [ "$opensslversion" = "2" ];then # 2-)OpenSSL 3 Latest(Compile From Source)
    sudo rm -rf /root/Downloads/openssl-latest
    sudo dnf -vy install perl gcc
    openssl_latest=$(lynx -dump https://www.openssl.org/source/ | awk '{print $2}' | grep -iv '.asc\|sha\|fips'\
    | grep -i .tar.gz | head -n 1)
    wget -O /root/Downloads/openssl-latest.tar.gz "$openssl_latest"
    sudo mkdir -pv /root/Downloads/openssl-latest
    tar -xvf /root/Downloads/openssl-latest.tar.gz -C /root/Downloads/openssl-latest --strip-components 1
    cd /root/Downloads/openssl-latest || exit 2
    sudo dnf -vy install perl-PathTools perl-FindBin perl-CPAN perl-IPC-Cmd gcc make perl pcre-devel zlib-devel
    ./config --prefix=/usr/local/openssl --openssldir=/usr/local/openssl shared zlib
    make -j "${core:=}" && make -j "${core:=}" install
    echo "export PATH=/usr/local/openssl/bin:$PATH" | sudo tee -a /etc/profile
    echo "export LD_LIBRARY_PATH=/usr/local/openssl/lib:$LD_LIBRARY_PATH" | sudo tee -a /etc/profile
    source /etc/profile
    sudo ln -sf /usr/local/openssl/bin/openssl /usr/bin/openssl
    sudo ln -sf /usr/local/openssl/lib/libssl.so /usr/lib64/libssl.so
    sudo ln -sf /usr/local/openssl/lib/libcrypto.so /usr/lib64/libcrypto.so
elif [ "$opensslversion" = "3" ];then # 3-)OpenSSL 3.0(LTS) (.rpm file from .spec)
    sudo dnf -vy install curl which make gcc perl perl-WWW-Curl rpm-build rpmdevtools rpmlint
    rpmdev-setuptree
    openssl_latest_lts=$(lynx -dump https://www.openssl.org/source/ | awk '{print $2}' | grep -iv '.asc\|sha\|fips' \
    | grep -i .tar.gz | grep -i 3.0. | head -n 1)
    openssl_latest_lts_version=$(lynx -dump https://www.openssl.org/source/ | awk '{print $2}' | grep -iv '.asc\|sha\|fips' \
    | grep -i .tar.gz | grep -i 3.0. | head -n 1 | sed -E 's/.*openssl-([0-9]+\.[0-9]+\.[0-9]+).*/\1/')
    wget -O /root/rpmbuild/SOURCES/openssl-"$openssl_latest_lts_version".tar.gz https://www.openssl.org/source/openssl-"$openssl_latest_lts_version".tar.gz
    sudo dnf -vy remove openssl openssl-devel
    cat << EOF > /root/rpmbuild/SPECS/openssl.spec
Summary: OpenSSL "$openssl_latest_lts_version" for Red Hat
Name: openssl
Version: %{?version}%{!?version:"$openssl_latest_lts_version"}
Release: 3%{?dist}
Obsoletes: %{name} <= %{version}
Provides: %{name} = %{version}
URL: https://www.openssl.org/
License: GPLv2+

Source: https://www.openssl.org/source/%{name}-%{version}.tar.gz

BuildRequires: make gcc perl perl-WWW-Curl
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
%global openssldir /usr/openssl

%description
https://github.com/philyuchkoff/openssl-RPM-Builder
OpenSSL RPM for version "$openssl_latest_lts_version" on Red Hat

%package devel
Summary: Development files for programs which will use the openssl library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
OpenSSL RPM for version "$openssl_latest_lts_version" on Red Hat (development package)

%prep
%setup -q

%build
./config --prefix=%{openssldir} --openssldir=%{openssldir}
make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%make_install

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}
ln -sf %{openssldir}/lib/libssl.so.1.1 %{buildroot}%{_libdir}
ln -sf %{openssldir}/lib/libcrypto.so.1.1 %{buildroot}%{_libdir}
ln -sf %{openssldir}/bin/openssl %{buildroot}%{_bindir}

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%{openssldir}
%defattr(-,root,root)

%files devel
%{openssldir}/include/*
%defattr(-,root,root)

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig
%define _unpackaged_files_terminate_build 0
EOF
    cd /root/rpmbuild/SPECS && \
    rpmbuild \
    -D 'debug_package %{nil}' \
    -ba openssl.spec
    sudo rpm -Uvh /root/rpmbuild/RPMS/x86_64/openssl-"$openssl_latest_lts_version"-1.el9.x86_64.rpm --nodeps
    sudo rpm -Uvh /root/rpmbuild/RPMS/x86_64/openssl-devel-"$openssl_latest_lts_version"-1.el8.x86_64.rpm
    ln -s /usr/openssl/lib64/libssl.so.3 /usr/lib64/libssl.so.3
    ln -s /usr/openssl/lib64/libcrypto.so.3 /usr/lib64/libcrypto.so.3
    ln -s /usr/openssl/bin/openssl /usr/bin/openssl
else
    echo "Out of options please choose between 1-6"
fi
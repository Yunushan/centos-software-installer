#!/bin/bash

# 26-GoCD

if [ "$gocd_version" = "1" ];then # 1-)GoCD Server
    #sudo dnf -vy install java-1.8.0-openjdk-devel
    #java -version
    sudo curl https://download.gocd.org/gocd.repo -o /etc/yum.repos.d/gocd.repo
    sudo dnf -vy install go-server --nogpgcheck
    sudo systemctl start go-server
    sudo systemctl enable go-server
    sudo mkdir -pv /opt/artifacts
    sudo chown -R go:go /opt/artifacts
elif [ "$gocd_version" = "2" ];then # 2-)GoCD Server (Docker)
    sudo dnf -y install dnf-plugins-core
    sudo dnf config-manager --add-repo https://download.docker.com/linux/rhel/docker-ce.repo
    sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    sudo systemctl enable --now docker
    sudo systemctl start docker
    docker run -d -p8153:8153 gocd/gocd-server:v25.1.0
elif [ "$gocd_version" = "3" ];then # 3-)GoCD Agent
    sudo curl https://download.gocd.org/gocd.repo -o /etc/yum.repos.d/gocd.repo
    sudo dnf -vy install go-agent
elif [ "$gocd_version" = "4" ];then # 4-)GoCD Agent (Docker)
    sudo dnf -y install dnf-plugins-core
    sudo dnf config-manager --add-repo https://download.docker.com/linux/rhel/docker-ce.repo
    sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    sudo systemctl enable --now docker
    sudo systemctl start docker
    printf "\nPlease Choose Your Desired GoCD Server URL\n\n1-)GoCD Server URL"
    read -r gocd_agent_server_url
    docker run -d -e GO_SERVER_URL=$gocd_agent_server_url gocd/gocd-agent-ubuntu-24.04:v25.1.0
else
    echo "Out of options please choose between 1-4"
fi
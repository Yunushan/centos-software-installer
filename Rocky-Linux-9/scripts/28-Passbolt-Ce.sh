#!/bin/bash

# 28-Passbolt-CE # 

if [ "$passbolt_version" = "1" ];then # 1-)GoCD Server
    curl -LO "https://download.passbolt.com/ce/installer/passbolt-repo-setup.ce.sh"
    curl -LO "https://github.com/passbolt/passbolt-dep-scripts/releases/latest/download/passbolt-ce-SHA512SUM.txt"
    sha512sum -c passbolt-ce-SHA512SUM.txt && sudo bash ./passbolt-repo-setup.ce.sh || echo "Bad checksum. Aborting" && rm -f passbolt-repo-setup.ce.sh
    sudo dnf install passbolt-ce-server
    sudo /usr/local/bin/passbolt-configure
    printf "\nPassbolt CE Installation Has Finished\n\n"
elif [ "$passbolt_version" = "2" ];then # 2-) Passbolt Community Edition (CE) Docker
    cd /root/Downloads/ || exit
    curl -LO https://download.passbolt.com/ce/docker/docker-compose-ce.yaml
    curl -LO https://github.com/passbolt/passbolt_docker/releases/latest/download/docker-compose-ce-SHA512SUM.txt
    sha512sum -c docker-compose-ce-SHA512SUM.txt
    docker compose -f docker-compose-ce.yaml up -d
    docker compose -f docker-compose-ce.yaml \
    exec passbolt su -m -c "/usr/share/php/passbolt/bin/cake \
        passbolt register_user \
        -u YOUR_EMAIL \
        -f YOUR_NAME \
        -l YOUR_LASTNAME \
        -r admin" -s /bin/sh www-data
else
    echo "Out of options please choose between 1-2"
fi
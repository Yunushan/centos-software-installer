#!/bin/bash

# 10-OpenJDK 8-11-17-21 JDK

if [ "$openjdkversion" = "1" ];then # 1-)OpenJDK 8
    sudo dnf -vy remove java-11-openjdk-devel
    sudo dnf -vy remove java-17-openjdk-devel
    sudo dnf -vy remove java-21-openjdk-devel
    sudo dnf -vy install java-1.8.0-openjdk-devel
    printf "\nOpenJDK 8 JDK Installation Has Finished \n\n"
elif [ "$openjdkversion" = "2" ];then # 2-)OpenJDK 11
    sudo dnf -vy remove java-17-openjdk-devel
    sudo dnf -vy remove java-1.8.0-openjdk-devel
    sudo dnf -vy remove java-21-openjdk-devel
    sudo dnf -vy install java-11-openjdk-devel
    printf "\nOpenJDK 11 JDK Installation Has Finished \n\n"
elif [ "$openjdkversion" = "3" ];then # 3-)OpenJDK 17
    sudo dnf -vy remove java-1.8.0-openjdk-devel
    sudo dnf -vy remove java-11-openjdk-devel
    sudo dnf -vy remove java-21-openjdk-devel
    sudo dnf -vy install java-17-openjdk-devel
    printf "\nOpenJDK 17 JDK Installation Has Finished \n\n"
elif [ "$openjdkversion" = "4" ];then # 4-)OpenJDK 21
    sudo dnf -vy remove java-1.8.0-openjdk-devel
    sudo dnf -vy remove java-11-openjdk-devel
    sudo dnf -vy remove java-17-openjdk-devel
    sudo dnf -vy install java-21-openjdk-devel
    printf "\nOpenJDK 21 JDK Installation Has Finished \n\n"
else
    echo "Out of options please choose between 1-4"
fi
#!/bin/bash

# 1-) Php

if [ "$php_version" = "1" ];then # 1-) PHP 8.0 (Red Hat Official Package)
    sudo dnf -vy install php php-cli php-common php-fpm php-mysqlnd php-xml php-curl php-gd \
    php-imagick php-mbstring php-opcache php-soap php-zip php-devel
elif [ "$php_version" = "2" ];then # 2-) PHP 7.4 (Remi Release)
    sudo dnf -vy install dnf-utils https://rpms.remirepo.net/enterprise/remi-release-9.rpm
    sudo dnf -vy install php74-php php74-php-cli php74-php-common php74-php-fpm php74-php-mysqlnd php74-php-xml \
    php74-php-curl php74-php-gd php74-php-imagick php74-php-mbstring php74-php-opcache php74-php-soap php74-php-zip \
    php74-php-devel
elif [ "$php_version" = "3" ];then # 3-) PHP 8.0 (Remi Release)
    sudo dnf -vy install dnf-utils https://rpms.remirepo.net/enterprise/remi-release-9.rpm
    sudo dnf -vy install php80-php php80-php-cli php80-php-common php80-php-fpm php80-php-mysqlnd php80-php-xml \
    php80-php-curl php80-php-gd php80-php-imagick php80-php-mbstring php80-php-opcache php80-php-soap php80-php-zip \
    php80-php-devel
elif [ "$php_version" = "4" ];then # 4-) PHP 8.1 (Remi Release)
    sudo dnf -vy install dnf-utils https://rpms.remirepo.net/enterprise/remi-release-9.rpm
    sudo dnf -vy install php81-php php81-php-cli php81-php-common php81-php-fpm php81-php-mysqlnd php81-php-xml \
    php81-php-curl php81-php-gd php81-php-imagick php81-php-mbstring php81-php-opcache php81-php-soap \
    php81-php-zip php81-php-devel
elif [ "$php_version" = "5" ];then # 5-) PHP 8.2 (Remi Release)
    sudo dnf -v install php82-php php82-php-cli php82-php-common php82-php-fpm php82-php-mysqlnd php82-php-xml \
    php82-php-curl php82-php-gd php82-php-mbstring php82-php-opcache php82-php-soap php82-php-zip php82-php-develx
elif [ "$php_version" = "6" ];then # 6-) PHP 8.3 (Remi Release)
    sudo dnf -vy install dnf-utils https://rpms.remirepo.net/enterprise/remi-release-9.rpm
    sudo dnf -vy install php83-php php83-php-cli php83-php-common php83-php-fpm php83-php-mysqlnd php83-php-xml \
    php83-php-curl php83-php-gd php83-php-imagick php83-php-mbstring php83-php-opcache php83-php-soap \
    php83-php-zip php83-php-devel
elif [ "$php_version" = "7" ];then # 7-) PHP 8.4 (Remi Release)
    sudo dnf -vy install dnf-utils https://rpms.remirepo.net/enterprise/remi-release-9.rpm
    sudo dnf -vy install php84-php php84-php-cli php84-php-common php84-php-fpm php84-php-mysqlnd php84-php-xml \
    php84-php-curl php84-php-gd php84-php-imagick php84-php-mbstring php84-php-opcache php84-php-soap \
    php84-php-zip php84-php-devel
else
    echo "Out of option Please Choose between 1-7"
fi
printf "\nPHP installation Has Finished\n\n"
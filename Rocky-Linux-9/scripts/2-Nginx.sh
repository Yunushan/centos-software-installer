#!/bin/bash

# 2-) Nginx installation section

if [ "$nginxversion" = "1" ];then # 1-) Nginx (Official Package)
    sudo dnf -vy install nginx
elif [ "$nginxversion" = "2" ];then # 2-) Nginx Latest(Mainline Compile From Source)
    sudo dnf -vy install gd gd-devel pcre-devel
    nginx_latest=$(lynx -dump http://nginx.org/en/download.html | awk '{print $2}' | grep -iv '.asc\|.zip' \
    | grep -i .tar.gz | head -n 1)
    mkdir -pv /root/Downloads/nginx_latest
    wget -O /root/Downloads/nginx_latest.tar.gz "$nginx_latest"
    tar -xvf /root/Downloads/nginx_latest/nginx_latest.tar.gz -C /root/Downloads/nginx_latest --strip-components 1
    cd /root/Downloads/nginx_latest
    ./configure --prefix=/var/www/html --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf \
    --http-log-path=/var/log/nginx/access.log --error-log-path=/var/log/nginx/error.log --with-pcre  \
    --lock-path=/var/lock/nginx.lock --pid-path=/var/run/nginx.pid --with-http_ssl_module \
    --with-http_image_filter_module=dynamic --modules-path=/etc/nginx/modules --with-http_v2_module \
    --with-stream=dynamic --with-http_addition_module --with-http_mp4_module
    make -j "$core" && make -j "$core" install

echo "[Unit]
Description=The NGINX HTTP and reverse proxy server
After=syslog.target network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/var/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
ExecReload=/usr/sbin/nginx -s reload
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target" > /lib/systemd/system/nginx.service
elif [ "$nginxversion" = "3" ];then # 3-) Nginx Stable(Compile From Source)
    sudo dnf -vy install yum-utils rpmdevtools rpm-build rpmdevtools rpmlint spectool
    sudo dnf -vy groupinstall "Development Tools"
    rpmdev-setuptree
    nginx_latest_source_rpm=$(lynx -dump http://nginx.org/packages/centos/9/SRPMS/ | awk '/http/ {print $2}' \
    | grep -iv 'perl\|njs\|xslt\|image-filter\|repodata\|module-otel' | grep -i el9.ngx.src.rpm | tail -n 1)
    sudo mkdir -pv /root/Downloads
    wget -O /root/Downloads/nginx-latest.ngx.src.rpm "$nginx_latest_source_rpm"
    sudo rpm -Uvh /root/Downloads/nginx-latest.ngx.src.rpm
    sudo yum-builddep -vy /root/Downloads/nginx-latest.ngx.src.rpm
    sudo rpmbuild -v --rebuild /root/Downloads/nginx-latest.ngx.src.rpm
    sudo rpm -Uvh /root/rpmbuild/RPMS/x86_64/nginx-*
elif [ "$nginxversion" = "4" ];then # 4-) Nginx (From .rpm File)
    nginx_latest_rpm=$(lynx -dump http://nginx.org/packages/centos/9/x86_64/RPMS/ | awk '/http/ {print $2}' \
    | grep -iv 'perl\|njs\|xslt\|image-filter\|repodata\|debuginfo\|module-otel' | grep -i .el9.ngx.x86_64.rpm | tail -n 1)
    sudo mkdir -pv /root/Downloads
    wget -O /root/Downloads/nginx-latest.rpm "$nginx_latest_rpm"
    sudo rpm -Uvh /root/Downloads/nginx-latest.rpm
elif [ "$nginxversion" = "5" ];then # 5-) Nginx (From nginx.list Stable)
    sudo dnf -vy install yum-utils
    echo "[nginx-stable]
name=nginx stable repo
baseurl=http://nginx.org/packages/centos/9/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true

[nginx-mainline]
name=nginx mainline repo
baseurl=http://nginx.org/packages/mainline/centos/9/x86_64/
gpgcheck=1
enabled=0
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true" > /etc/yum.repos.d/nginx.repo
    sudo yum-config-manager -v --disable nginx-mainline
    sudo yum-config-manager -v --enable nginx-stable
    sudo dnf -vy install nginx
elif [ "$nginxversion" = "6" ];then # 6-) Nginx (From nginx.list Mainline)
    sudo dnf -vy install yum-utils
    echo "[nginx-stable]
name=nginx stable repo
baseurl=http://nginx.org/packages/centos/9/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true

[nginx-mainline]
name=nginx mainline repo
baseurl=http://nginx.org/packages/mainline/centos/9/x86_64/
gpgcheck=1
enabled=0
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true" > /etc/yum.repos.d/nginx.repo
    sudo yum-config-manager -v --disable nginx-stable
    sudo yum-config-manager -v --enable nginx-mainline
    sudo dnf -vy install nginx
else
    echo "Out of options please choose between 1-6"
fi
sudo systemctl enable nginx
sudo systemctl start nginx
#sudo firewall-cmd --permanent --zone=public --add-service=http
#sudo firewall-cmd --permanent --zone=public --add-service=https
#sudo firewall-cmd --reload

printf "\nNginx Installation Has Finished\n\n"
#!/bin/bash

# 6-) FFmpeg

if [ "$ffmpeg_version" = "1" ];then # 1-)FFmpeg (Official Package)
    mkdir -pv /root/Downloads/ffmpeglatest/
    cd /root/Downloads/ffmpeglatest/ || exit 2
    make -j "${core:=}" uninstall
    snap remove ffmpeg
    dnf config-manager --enable crb
    sudo dnf -vy install ffmpeg ffmpeg-devel ffmpeg-libs
elif [ "$ffmpeg_version" = "2" ];then # 2-)FFmpeg Latest(Compile From Source)
    mkdir -pv /root/Downloads/ffmpeglatest/
    sudo dnf -vy install gcc
    sudo snap remove ffmpeg
    sudo dnf -vy remove ffmpeg ffmpeg-devel ffmpeg-libs
    sudo mkdir -pv /root/Downloads/ffmpeglatest
    ffmpeg_latest=$(lynx -dump https://www.ffmpeg.org/releases/ | awk '/http/{print $2}' | grep -iv '.asc\|md5\|snapshot' | \
    grep -i tar.gz | tail -n 1)
    sudo wget -O /root/Downloads/ffmpeglatest.tar.gz "$ffmpeg_latest"
    tar xvf /root/Downloads/ffmpeglatest.tar.gz -C /root/Downloads/ffmpeglatest --strip-components 1
    cd /root/Downloads/ffmpeglatest/ || exit 2
    ./configure --disable-x86asm
    make -j "${core:=}" && make -j "${core:=}" install
elif [ "$ffmpeg_version" = "3" ];then # 3-)FFmpeg Latest(Snap)
    mkdir -pv /root/Downloads/ffmpeglatest/
    sudo dnf -vy remove ffmpeg ffmpeg-devel ffmpeg-libs
    cd /root/Downloads/ffmpeglatest/ || exit 2
    make -j "${core:=}" uninstall
    sudo snap install ffmpeg
else 
    echo "Out of option Please Choose between 1-3"
fi
#!/bin/bash

# 15-Nodejs-And-Npm

if [ "$nodejsversion" = "1" ];then # Official Repo
    sudo dnf -vy remove nodejs
    sudo dnf -vy module disable nodejs:22
    sudo dnf -vy module disable nodejs:20
    sudo dnf -vy module disable nodejs:18
    sudo dnf -vy install nodejs
    node --version
elif [ "$nodejsversion" = "2" ];then # LTS 22
    sudo dnf -vy remove nodejs
    sudo dnf -vy module disable nodejs:20
    sudo dnf -vy module disable nodejs:18
    sudo dnf -vy module enable nodejs:22
    sudo dnf -vy install nodejs
    node --version
elif [ "$nodejsversion" = "3" ];then # LTS (20)
    sudo dnf -vy remove nodejs 
    sudo dnf -vy module disable nodejs:22
    sudo dnf -vy module disable nodejs:18
    sudo dnf -vy module enable nodejs:20
    sudo dnf -vy install nodejs
    node --version
elif [ "$nodejsversion" = "4" ];then # LTS (18)
    sudo dnf -vy remove nodejs
    sudo dnf -vy module disable nodejs:22
    sudo dnf -vy module disable nodejs:20
    sudo dnf -vy module enable nodejs:18
    sudo dnf -vy install nodejs
    node --version
else
    echo "Out of options please choose between 1-4"
fi
#!/bin/bash

# 30-Tinc

if [ "$tincversion" = "1" ];then # 1-) Tinc (From Official Repository)
    cd /root/Downloads/tinc-latest && make -j "${core:=}" uninstall
    cd /root/Downloads/tinc-latest-pre && make -j "$core" uninstall
    sudo dnf -vy remove tinc
    sudo dnf -vy install tinc
elif [ "$tincversion" = "2" ];then # 2-) Tinc Latest Stable(Compile From Source)
    cd /root/Downloads/tinc-latest && make -j "$core" uninstall
    cd /root/Downloads/tinc-latest-pre && make -j "$core" uninstall
    sudo dnf -vy remove tinc
    sudo dnf -vy install zlib zlib-devel lzo lzo-devel openssl openssl-devel
    tinclatest=$(lynx -dump https://www.tinc-vpn.org/download/ | awk '/http/{print $2}' | grep -iv '.sig\|pre' \
    | grep -i .tar.gz | head -n 1)
    wget -O /root/Downloads/tinc-latest.tar.gz "$tinclatest"
    sudo mkdir -pv /root/Downloads/tinc-latest
    tar -xvf /root/Downloads/tinc-latest.tar.gz -C /root/Downloads/tinc-latest --strip-components 1
    cd /root/Downloads/tinc-latest || exit 2
    ./configure
    make -j "$core" && make -j "$core" install
    tincd --version
elif [ "$tincversion" = "3" ];then # 3-) Tinc Latest Pre-Release 1.1(Compile From Source)
    cd /root/Downloads/tinc-latest && make -j "$core" uninstall
    cd /root/Downloads/tinc-latest-pre && make -j "$core" uninstall
    sudo dnf -vy remove tinc
    tinclatestpre=$(lynx -dump https://www.tinc-vpn.org/download/ | awk '/http/{print $2}' | grep -i 'pre' \
    | grep -i .tar.gz | grep -iv .sig | head -n 1)
    wget -O /root/Downloads/tinc-latest-pre.tar.gz "$tinclatestpre"
    sudo mkdir -pv /root/Downloads/tinc-latest-pre
    tar -xvf /root/Downloads/tinc-latest-pre.tar.gz -C /root/Downloads/tinc-latest-pre --strip-components 1
    cd /root/Downloads/tinc-latest-pre || exit 2
    ./configure
    make -j "$core" && make -j "$core" install
    tincd --version
elif [ "$tincversion" = "4" ];then # 4-) Tinc Snap
    cd /root/Downloads/tinc-latest && make -j "$core" uninstall
    cd /root/Downloads/tinc-latest-pre && make -j "$core" uninstall
    sudo dnf -vy remove tinc
    sudo snap install tinc-vpn
    tinc-vpn.tincd --version
else
    echo "Out of options please choose between 1-4"
fi
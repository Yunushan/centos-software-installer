#!/bin/bash

# Variables
cpuarch=$(uname -m)
core=$(nproc)
snap_path_is_include=$(export PATH="$PATH:/snap/bin/")
scripts_path=$(find / -name scripts | grep -i "Rocky-Linux-9/scripts" | head -n 1)

# Select Which Softwares to be Installed

choice () {
    local choice=$1
    if [[ ${opts[choice]} ]] # toggle
    then
        opts[choice]=
    else
        opts[choice]=+
    fi
}
PS3='
Please enter your choice(s): '
while :
do
    clear
    options=("PHP ${opts[1]}" "Nginx ${opts[2]}" "Apache ${opts[3]}" "Grub Customizer ${opts[4]}" "Linux Kernel ${opts[5]}"
    "FFmpeg ${opts[6]}" "OpenSSL ${opts[7]}" "OpenSSH ${opts[8]}" "Mysql ${opts[9]}" "OpenJDK 8-11-17-21 ${opts[10]}"
    "DVBlast 3.4 ${opts[11]}" "Zabbix Server ${opts[12]}" "UrBackup Server ${opts[13]}" "PostgreSQL ${opts[14]}"
    "Nodejs-And-Npm ${opts[15]}" "Winehq ${opts[16]}" "Pgadmin ${opts[17]}" "Pgagent ${opts[18]}" "Wazuh Server ${opts[19]}"
    "Phpmyadmin ${opts[20]}" "Elasticsearch ${opts[21]}" "Logstash ${opts[22]}" "Kibana ${opts[23]}"
    "Google-Authenticator ${opts[24]}" "Vim ${opts[25]}" "Gocd ${opts[26]}" "Jenkins ${opts[27]}" "Passbolt-Ce ${opts[28]}"
    "Fail2ban ${opts[29]}" "Tinc ${opts[30]}" "Nmap ${opts[31]}" "Done ${opts[32]}")
    select opt in "${options[@]}";do
        case $opt in
            "PHP ${opts[1]}")
                choice 1
                break
                ;;
            "Nginx ${opts[2]}")
                choice 2
                break
                ;;
            "Apache ${opts[3]}")
                choice 3
                break
                ;;
            "Grub Customizer ${opts[4]}")
                choice 4
                break
                ;;
            "Linux Kernel ${opts[5]}")
                choice 5
                break
                ;;
            "FFmpeg ${opts[6]}")
                choice 6
                break
                ;;
            "OpenSSL ${opts[7]}")
                choice 7
                break
                ;;
            "OpenSSH ${opts[8]}")
                choice 8
                break
                ;;
            "Mysql ${opts[9]}")
                choice 9
                break
                ;;
            "OpenJDK 8-11-17-21 ${opts[10]}")
                choice 10
                break
                ;;
            "DVBlast 3.4 ${opts[11]}")
                choice 11
                break
                ;;
            "Zabbix Server ${opts[12]}")
                choice 12
                break
                ;;
            "UrBackup Server ${opts[13]}")
                choice 13
                break
                ;;
            "PostgreSQL ${opts[14]}")
                choice 14
                break
                ;;
            "Nodejs-And-Npm ${opts[15]}")
                choice 15
                break
                ;;
            "Winehq ${opts[16]}")
                choice 16
                break
                ;;
            "Pgadmin ${opts[17]}")
                choice 17
                break
                ;;
            "Pgagent ${opts[18]}")
                choice 18
                break
                ;;
            "Wazuh Server ${opts[19]}")
                choice 19
                break
                ;;
            "Phpmyadmin ${opts[20]}")
                choice 20
                break
                ;;
            "Elasticsearch ${opts[21]}")
                choice 21
                break
                ;;
            "Logstash ${opts[22]}")
                choice 22
                break
                ;;
            "Kibana ${opts[23]}")
                choice 23
                break
                ;;
            "Google-Authenticator ${opts[24]}")
                choice 24
                break
                ;;
            "Vim ${opts[25]}")
                choice 25
                break
                ;;
            "Gocd ${opts[26]}")
                choice 26
                break
                ;;
            "Jenkins ${opts[27]}")
                choice 27
                break
                ;;
            "Passbolt-Ce ${opts[28]}")
                choice 28
                break
                ;;
            "Fail2ban ${opts[29]}")
                choice 29
                break
                ;;
            "Tinc ${opts[30]}")
                choice 30
                break
                ;;
            "Nmap ${opts[31]}")
                choice 31
                break
                ;;
            "Done ${opts[32]}")
                break 2
                ;;
            *) printf '%s\n' 'Please Choose Between 1-32';;
        esac
    done
done
#Options Chosen For Block
printf '%s\n\n' 'Options chosen:'
for opt in "${!opts[@]}";do
    if [[ ${opts[opt]} ]];then
        if [ "$opt" == 1 ];then # 1-Php
            printf "\nPlease Choose Your Desired PHP Version \n\n1-) PHP 8.0 (Red Hat Official Package)\n\
2-) PHP 7.4 (Remi Release)\n3-) PHP 8.0 (Remi Release)\n4-) PHP 8.1 (Remi Release)\n5-) PHP 8.2 (Remi Release)\n6-) PHP 8.3 (Remi Release)\n\
7-) PHP 8.4 (Remi Release)\n\nPlease Select Your PHP Version:"
            while true;do
                read -r php_version
                if [[ $php_version = "1" ]] || [[ $php_version = "2" ]] || [[ $php_version = "3" ]] || [[ $php_version = "4" ]] \
                || [[ $php_version = "5" ]] || [[ $php_version = "6" ]] || [[ $php_version = "7" ]];then
                    break
                else
                    printf "Please Input Between 1-7:"
                fi
            done
        elif [ "$opt" = 2 ];then # 2-Nginx
            printf "\nPlease Choose Your Desired Nginx Version\n\n1-) Nginx (Official Package)\n\
2-) Nginx Latest(Mainline Compile From Source)\n3-) Nginx Stable(Compile From Source)\n\
4-) Nginx (From .rpm File)\n5-) Nginx (From nginx.list Stable)\n6-) Nginx (From nginx.list Mainline)\n\n\
Please Select Your Nginx Version(Please Input Between 1-6):" 
            while true;do
                read -r nginxversion
                if [[ $nginxversion = "1" ]] || [[ $nginxversion = "2" ]] || [[ $nginxversion = "3" ]] || [[ $nginxversion = "4" ]] \
                || [[ $nginxversion = "5" ]] || [[ $nginxversion = "6" ]];then
                    printf "Chosen option $nginxversion\n"
                    break
                else
                    printf "Please Input Between 1-6:"
                fi
            done
        elif [ "$opt" = 3 ];then # 3-) Apache
            printf "\nPlease Choose Your Desired Apache Version\n\n1-) Apache (From Official Package)\n\
2-) Apache Latest(Compile From Source)\n3-) Apache Latest (From .rpm file with default spec file)\n\
4-) Apache Latest (From .rpm file with custom .spec file only --prefix=/etc/httpd added)\
\n\nPlease Select Your Apache Version:"
            while true;do
                read -r apacheversion
                if [[ $apacheversion = "1" ]] || [[ $apacheversion = "2" ]] || [[ $apacheversion = "3" ]] || [[ $apacheversion = "4" ]];then
                    printf "Chosen option $apacheversion\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 5 ];then # 5-) Linux Kernel
            printf "\nPlease Choose Your Desired Kernel Version\n\n1-)Mainline Kernel (Latest)\n\
2-)Longterm Kernel (LTS)\n\nPlease Select Your Linux Kernel Version:"
            while true;do
                read -r kernelversion
                if [[ $kernelversion = "1" ]] || [[ $kernelversion = "2" ]];then
                    printf "Chosen option $kernelversion\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 6 ];then # 6-) Ffmpeg
            printf "\nPlease Choose Your Desired FFmpeg Version \n\n1-)FFmpeg (Official Package)\n2-)FFmpeg Latest(Compile From Source)\n\
3-)FFmpeg Latest(Snap)\n\nPlease Select Your FFmpeg Version:"
            while true;do
                read -r ffmpeg_version
                if [[ $ffmpeg_version = "1" ]] || [[ $ffmpeg_version = "2" ]] || [[ $ffmpeg_version = "3" ]];then
                    printf "Chosen option $ffmpeg_version\n"
                    break
                else
                    printf "Please Input Between 1-3:"
                fi
            done
        elif [ "$opt" = 7 ];then # 7-) Openssl
            printf "\nPlease Choose Your Desired OpenSSL Version\n\n1-)OpenSSL 3 LTS (Official Package)\n\
2-)OpenSSL 3 Latest(Compile From Source)\n3-)OpenSSL 3.0(LTS) (.rpm file from .spec)\n
Please Select Your OpenSSL Version:"
            while true;do
                read -r opensslversion
                if [[ $opensslversion = "1" ]] || [[ $opensslversion = "2" ]] || [[ $opensslversion = "3" ]];then
                    printf "Chosen option $opensslversion\n"
                    break
                else
                    printf "Please Input Between 1-3:"
                fi
            done
        elif [ "$opt" = 8 ];then # 8-) Openssh
            printf "\nPlease Choose Your Desired OpenSSH Version\n\n1-) OpenSSH (Official Package)\n\
2-) OpenSSH Latest (Compile From Source)\n3-) OpenSSH Latest (.rpm file from .spec)\n\nPlease Select Your OpenSSH Version(Please Input Between 1-3):"
            while true;do
                read -r openssh_version
                if [[ $openssh_version = "1" ]] || [[ $openssh_version = "2" ]] || [[ $openssh_version = "3" ]];then
                    printf "Chosen option $openssh_version\n"
                    break
                else
                    printf "Please Input Between 1-3:"
                fi
            done
        elif [ "$opt" = 9 ];then # 9-) Mysql
            printf "\nPlease Choose Your Desired Mysql Version\n\n1-)Mysql 8.0 (Official Package)\n2-)Mysql 5.7\n3-)Mysql 5.6\n\
4-)Mysql 5.5\n5-)Mysql 8.4(LTS) (Latest)\n\nPlease Select Your Mysql Version:"
            while true;do
                read -r mysqlversion
                if [[ $mysqlversion = "1" ]] || [[ $mysqlversion = "2" ]] || [[ $mysqlversion = "3" ]] || [[ $mysqlversion = "4" ]] \
                || [[ $mysqlversion = "5" ]];then
                    printf "Chosen option $mysqlversion\n"
                    break
                else
                    printf "Please Input Between 1-5:"
                fi
            done
        elif [ "$opt" = 10 ];then # 10-) Openjdk
            printf "\nPlease Choose Your Desired OpenJDK Version\n\n1-)OpenJDK 8 \n2-)OpenJDK 11\n\
3-)OpenJDK 17\n4-)OpenJDK 21\n\nPlease Select Your OpenJDK Version:"
            while true;do
                read -r openjdkversion
                if [[ $openjdkversion = "1" ]] || [[ $openjdkversion = "2" ]] || [[ $openjdkversion = "3" ]] || [[ $openjdkversion = "4" ]];then
                    printf "Chosen option $openjdkversion\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 12 ];then # 12-) Zabbix Server
            printf "\nPlease Choose Your Desired Zabbix Option\n\n1-) Zabbix Server 6.0 LTS (Mysql & Apache)\n\
2-) Zabbix Server 6.0 LTS (Mysql & Nginx)\n3-) Zabbix Server 6.0 LTS (PostgreSQL & Apache)\n\
4-) Zabbix Server 6.0 LTS (PostgreSQL & Nginx)\n5-) Zabbix Server 7.0 LTS (Mysql & Apache)\n\
6-) Zabbix Server 7.0 LTS (Mysql & Nginx)\n7-) Zabbix Server 7.0 LTS (PostgreSQL & Apache)\n\
8-) Zabbix Server 7.0 LTS (PostgreSQL & Nginx)\n\nPlease Select Your Zabbix Option:"
            while true;do
                read -r zabbix_option
                if [[ $zabbix_option = "1" ]] || [[ $zabbix_option = "2" ]];then
                    printf "Chosen option $zabbix_option\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 13 ];then # 13-) Urbackup Server
            printf "\nPlease Choose Your UrBackup Version \n\n1-)UrBackup Server Latest (Compile From Source)\n2-)UrBackup Server (Docker)\n\n\
Please Select Your UrBackup Version:"
            while true;do
                read -r urbackup_version
                if [[ $urbackup_version = "1" ]] || [[ $urbackup_version = "2" ]];then
                    printf "Chosen option $urbackup_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 14 ];then # 14-) Postgresql
            printf "\nPlease Choose Your Desired PostgreSQL Version\n\n1-)PostgreSQL ""$postgresql_version""\n2-)PostgreSQL ""$postgresql_version""\n\
3-)PostgreSQL ""$postgresql_version""\n4-)PostgreSQL ""$postgresql_version""\n5-)PostgreSQL ""$postgresql_version""\n\nPlease Select Your PostgreSQL Version:"
            while true;do
                read -r postgresql_version
                if [[ $postgresql_version = "1" ]] || [[ $postgresql_version = "2" ]] || [[ $postgresql_version = "3" ]] || [[ $postgresql_version = "4" ]];then
                    printf "Chosen option $postgresql_version\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 15 ];then # 15-) Nodejs And Npm
            printf "\nPlease Choose Your Desired Nodejs Version\n\n1-)Nodejs (Official Repo)\n2-)Nodejs 22 (LTS)\n\
3-)Nodejs 20 (LTS)\n4-)Nodejs 18 (LTS)\n\nPlease Select Your Nodejs Version:"
            while true;do
                read -r nodejsversion
                if [[ $nodejsversion = "1" ]] || [[ $nodejsversion = "2" ]] || [[ $nodejsversion = "3" ]] || [[ $nodejsversion = "4" ]];then
                    printf "Chosen option $nodejsversion\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 16 ];then # 16-) Winehq
            printf "\nPlease Choose Your Desired WineHQ Version \n\n1-) WineHQ (Official Package)\n2-) WineHQ (Compile Latest Version)\n\n
Please Select Your WineHQ Version:"
            while true;do
                read -r winehq_version
                if [[ $winehq_version = "1" ]] || [[ $winehq_version = "2" ]];then
                    printf "Chosen option $winehq_version\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 18 ];then # 18-) Pgagent
            pgagent_version_1=$(lynx -dump https://download.postgresql.org/pub/repos/yum/ | awk '/http/{print $2}' \
            | grep -iv "common\|debug\|keys\|non-free\|reporpms\|srpms\|testing" | tail -n 5 | sed -n '1 p' | cut -c 47-48)
            pgagent_version_2=$(lynx -dump https://download.postgresql.org/pub/repos/yum/ | awk '/http/{print $2}' \
            | grep -iv "common\|debug\|keys\|non-free\|reporpms\|srpms\|testing" | tail -n 5 | sed -n '2 p' | cut -c 47-48)
            pgagent_version_3=$(lynx -dump https://download.postgresql.org/pub/repos/yum/ | awk '/http/{print $2}' \
            | grep -iv "common\|debug\|keys\|non-free\|reporpms\|srpms\|testing" | tail -n 5 | sed -n '3 p' | cut -c 47-48)
            pgagent_version_4=$(lynx -dump https://download.postgresql.org/pub/repos/yum/ | awk '/http/{print $2}' \
            | grep -iv "common\|debug\|keys\|non-free\|reporpms\|srpms\|testing" | tail -n 5 | sed -n '4 p' | cut -c 47-48)
            pgagent_version_5=$(lynx -dump https://download.postgresql.org/pub/repos/yum/ | awk '/http/{print $2}' \
            | grep -iv "common\|debug\|keys\|non-free\|reporpms\|srpms\|testing" | tail -n 5 | sed -n '5 p' | cut -c 47-48)
printf "\nPlease Choose Your Desired PgAgent Version\n\n1-)PgAgent ""$pgagent_version_1""\n2-)PgAgent ""$pgagent_version_2""\n\
3-)PgAgent ""$pgagent_version_3""\n4-)PgAgent ""$pgagent_version_4""\n5-)PgAgent ""$pgagent_version_5""\n\nPlease Select Your PgAgent Version:"
            while true;do
                read -r pgagent_version
                if [[ $pgagent_version = "1" ]] || [[ $pgagent_version = "2" ]] || [[ $pgagent_version = "3" ]] || [[ $pgagent_version = "4" ]]\
                || [[ $pgagent_version = "4" ]];then
                    printf "Chosen option $pgagent_version\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 19 ];then # 19-) Wazuh Server
            printf "\nPlease Choose Your Desired Wazuh Server Installation\n\n1-) Wazuh Server (Assistant Installation)\n\
2-) Wazuh Server (Step-By-Step Installation)\n\nPlease Select Your Wazuh Server Version:"
            while true;do
                read -r wazuh_server_version
                if [[ $wazuh_server_version = "1" ]] || [[ $wazuh_server_version = "2" ]];then
                    printf "Chosen option $wazuh_server_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 20 ];then # 20-) Phpmyadmin
            printf "\nPlease Choose Your Desired PhpMyAdmin Version\n\n1-) PhpMyAdmin (For Apache(httpd))\n\
2-) PhpMyAdmin (For Nginx)\n\nPlease Select Your PhpMyAdmin Version:"
            while true;do
                read -r phpmyadmin_version
                if [[ $phpmyadmin_version = "1" ]] || [[ $phpmyadmin_version = "2" ]];then
                    printf "Chosen option $phpmyadmin_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 21 ];then # 21-) Elasticsearch
            printf "\nPlease Choose Your Desired Elasticsearch Version\n\n1-) Elasticsearch(From Official Package)\n\
2-) Elasticsearch (Docker)\n\nPlease Select Your Elasticsearch Version:"
            while true;do
                read -r jenkins_version
                if [[ $elasticsearch_version = "1" ]] || [[ $elasticsearch_version = "2" ]];then
                    printf "Chosen option $elasticsearch_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 22 ];then # 22-) Logstash
            printf "\nPlease Choose Your Desired Logstash Version\n\n1-) Logstash (From Official Package)\n\
2-) Logstash (Via Docker)\n3-) Logstash (From .rpm file)\n4-) Logstash Source File\n\nPlease Select Your Logstash Version:"
            while true;do
                read -r jenkins_version
                if [[ $logstash_version = "1" ]] || [[ $logstash_version = "2" ]] || [[ $logstash_version = "3" ]] || [[ $logstash_version = "4" ]];then
                    printf "Chosen option $logstash_version\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 23 ];then # 23-) Kibana
            printf "\nPlease Choose Your Desired Kibana Version\n\n1-) Kibana (with repository package manager)\n\
2-) Kibana (Via Docker)\n3-) Kibana (From .rpm file)\n\nPlease Select Your Kibana Version:"
            while true;do
                read -r kibana_version
                if [[ $kibana_version = "1" ]] || [[ $kibana_version = "2" ]] || [[ $kibana_version = "3" ]];then
                    printf "Chosen option $kibana_version\n"
                    break
                else
                    printf "Please Input Between 1-3:"
                fi
            done
        elif [ "$opt" = 24 ];then # 24-) Google Authenticator
            printf "\nPlease Choose Your Desired Google Authenticator Version\n1-)Google Authenticator (Compile From Source)\n\
2-)Google Authenticator (From .rpm file)\n\nPlease Select Your Google Authenticator Version:"
            while true;do
                read -r google_authenticator_version
                if [[ $google_authenticator_version = "1" ]] || [[ $google_authenticator_version = "2" ]];then
                    printf "Chosen option $google_authenticator_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 25 ];then # 25-) Vim
            printf "\nPlease Choose Your Desired Vim Version\n1-)Vim Version (Official Package Manager)\n\
2-)Vim Latest (Compile From Source)\n\nPlease Select Your Vim Version:"
            while true;do
                read -r vim_version
                if [[ $vim_version = "1" ]] || [[ $vim_version = "2" ]];then
                    printf "Chosen option $vim_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 26 ];then # 26-) Gocd
            printf "\nPlease Choose Your GoCD Version \n\n1-)GoCD Server\n2-)GoCD Server (Docker)\n3-)GoCD Agent\n\
4-)GoCD Agent (Docker)\n\nPlease Select Your GoCD Version:"
            while true;do
                read -r gocd_version
                if [[ $gocd_version = "1" ]] || [[ $gocd_version = "2" ]] || [[ $gocd_version = "3" ]] || [[ $gocd_version = "4" ]];then
                    printf "Chosen option $gocd_version\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 28 ];then # 28-) Passbolt Community Edition
            printf "\nPlease Choose Your Desired Passbolt Community Edition (CE) Version\n\n1-) Passbolt Community Edition (CE) Package Manager\n\
2-) Passbolt Community Edition (CE) Docker\nPlease Select Your Passbolt Community Edition (CE) Version(Please Input Between 1-4):"
            while true;do
                read -r passbolt_version
                if [[ $passbolt_version = "1" ]] || [[ $passbolt_version = "2" ]];then
                    printf "Chosen option $passbolt_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 30 ];then # 30-) Tinc
            printf "\nPlease Choose Your Desired Tinc Version\n\n1-) Tinc (From Official Repository)\n\
2-) Tinc Latest Stable(Compile From Source)\n3-) Tinc Latest Pre-Release 1.1(Compile From Source)\n4-) Tinc Snap\n\nPlease Select Your Tinc:"
            while true;do
                read -r tincversion
                if [[ $tincversion = "1" ]] || [[ $tincversion = "2" ]] || [[ $tincversion = "3" ]] || [[ $tincversion = "4" ]];then
                    printf "Chosen option $tincversion\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 31 ];then # 31-) Nmap
            printf "\nPlease Choose Your Desired Nmap Version\n\n1-) Nmap (From Official Repository)\n\
2-) Nmap Latest(From .rpm File)\n3-) Nmap Latest (Compile From Source)\n\nPlease Select Your Nmap:"
            while true;do
                read -r nmap_version
                if [[ $nmap_version = "1" ]] || [[ $nmap_version = "2" ]] || [[ $nmap_version = "3" ]];then
                    printf "Chosen option $nmap_version\n"
                    break
                else
                    printf "Please Input Between 1-3:"
                fi
            done
        else
            :
        fi
    # End Of Control For Inner Scripts
    fi
done

if [ "${opts[opt]}" = "" ];then
    exit
fi



# Loading Bar

printf "Installation starting"
value=0
while [ $value -lt 600 ];do
    value=$((value+20))
    printf "."
    sleep 0.05
done
printf "\n"


#Necessary Packages
CODEREADY_BUILDER=$(yum repolist | grep -qi "codeready-builder-for-rhel")
if [ -z "$CODEREADY_BUILDER" ];then
    :
else
    sudo subscription-manager repos --enable codeready-builder-for-rhel-9-x86_64-rpm
fi

sudo dnf -vy install https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
sudo dnf -vy install --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-9.noarch.rpm \
https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-9.noarch.rpm
sudo dnf -vy install wget curl mlocate nano lynx net-tools htop git dnf yum snapd bash-completion dnf-utils shellcheck rpmdevtools
sudo systemctl enable --now snapd.socket
sudo ln -s /var/lib/snapd/snap /snap
sudo systemctl start snapd
source /etc/profile
source /etc/profile.d/bash_completion.sh
printf "\n"
local_ip=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')

# Create Download Folder in root
if [ -d "/root/Downloads/" ];then
    :
else
    sudo mkdir -pv /root/Downloads/
fi

# INSTALLATION BY SELECTION

for opt in "${!opts[@]}"
do
    if [[ ${opts[opt]} ]]
    then
        case $opt in 
            1) 
            #PHP
            . "$scripts_path/1-Php.sh"
            ;;
            2)
            # 2- Nginx
            . "$scripts_path/2-Nginx.sh"
            ;;
            3)
            # 3- Apache
            . "$scripts_path/3-Apache.sh"
            ;;
            4)
            # 4- Grub Customizer
            . "$scripts_path/4-Grub-Customizer.sh"
            ;;
            5)
            # 5-Linux Kernel
            . "$scripts_path/5-Linux-Kernel.sh"
            ;;
            6)
            # 6-FFmpeg
            . "$scripts_path/6-Ffmpeg.sh"
            ;;
            7)
            # 7-OpenSSL
            . "$scripts_path/7-Openssl.sh"
            ;;
            8)
            # 8-OpenSSH
            . "$scripts_path/8-Openssh.sh"
            ;;
            9)
            # 9-Mysql
            . "$scripts_path/8-Openssh.sh"
            ;;
            10)
            # 10-OpenJDK 8-11-17-21
            . "$scripts_path/10-Openjdk.sh"
            ;;
            11)
            # 11-DVBlast 3.4
            . "$scripts_path/11-Dvblast.sh"
            ;;
            12)
            # 12-Zabbix Server
            . "$scripts_path/12-Zabbix-Server.sh"
            ;;
            13)
            # 13-UrBackup Server
            . "$scripts_path/13-Urbackup-Server.sh"
            ;;
            14)
            # 14-PostgreSQL
            . "$scripts_path/14-Postgresql.sh"
            ;;
            15)
            # 15-Nodejs-And-Npm
            . "$scripts_path/15-Nodejs-And-Npm.sh"
            ;;
            16)
            # 16-Winehq
            . "$scripts_path/16-Winehq.sh"
            ;;
            17)
            # 17-Pgadmin
            . "$scripts_path/17-Pgadmin.sh"
            ;;
            18)
            # 18-Pgagent
            . "$scripts_path/18-Pgagent.sh"
            ;;
            19)
            # 19-Wazuh-Server
            . "$scripts_path/19-Wazuh-Server.sh"
            ;;
            20)
            # 20-Phpmyadmin
            . "$scripts_path/20-Phpmyadmin.sh"
            ;;
            21)
            # 21-Elasticsearch
            . "$scripts_path/21-Elasticsearch.sh"
            ;;
            22)
            # 22-Logstash
            . "$scripts_path/22-Logstash.sh"
            ;;
            23)
            # 23-Kibana
            . "$scripts_path/23-Kibana.sh"
            ;;
            24)
            # 24-Google-Authenticator
            . "$scripts_path/24-Google-Authenticator.sh"
            ;;
            25)
            # 25-Vim
            . "$scripts_path/25-Vim.sh"
            ;;
            26)
            # 26-Gocd
            . "$scripts_path/26-Gocd.sh"
            ;;
            27)
            # 27-Jenkins
            . "$scripts_path/27-Jenkins.sh"
            ;;
            28)
            # 28-Passbolt-Ce
            . "$scripts_path/28-Passbolt-Ce.sh"
            ;;
            29)
            # 29-Fail2ban
            . "$scripts_path/29-Fail2ban.sh"
            ;;
            30)
            # 30-Tinc
            . "$scripts_path/30-Tinc.sh"
            ;;
            31)
            # 31-Tinc
            . "$scripts_path/31-Nmap.sh"
            ;;
        esac
    fi
done